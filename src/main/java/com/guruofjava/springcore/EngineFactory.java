package com.guruofjava.springcore;

public class EngineFactory {
    public Engine createEngine(){
        Engine temp = new Engine();
        temp.setBrand("Ford");
        temp.setHp(50 + Math.random() * 40);
        return temp;
    }
    
    public Car createCar(){
        return new Car();
    }
}
