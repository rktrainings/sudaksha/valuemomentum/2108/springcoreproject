package com.guruofjava.springcore;

public class Engine {

    private String brand;
    private double hp;
    
    public Engine(){
        brand = "Ford";
        hp = 40;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    @Override
    public String toString() {
        return "Engine{" + "brand=" + brand + ", hp=" + hp + '}';
    }

    public static Engine getInstance() {
        Engine temp = new Engine();
        temp.setBrand("Ford");
        temp.setHp(20 + Math.random() * 40);
        return temp;
    }
}
