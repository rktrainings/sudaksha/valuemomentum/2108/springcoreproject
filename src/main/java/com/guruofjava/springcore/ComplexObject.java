package com.guruofjava.springcore;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class ComplexObject {
    private Properties adminEmails = new Properties();
    private List someList = new ArrayList();
    private Map someMap = new LinkedHashMap();
    private Set someSet = new LinkedHashSet();
}
