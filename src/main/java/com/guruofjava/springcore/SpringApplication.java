package com.guruofjava.springcore;

import com.guruofjava.springcore.user.UserController;
import com.guruofjava.springcore.user.UserRepository;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan(basePackages = {"com.guruofjava.springcore"})
//@PropertySource(value = {"classpath:application.properties"})
@PropertySources({
    @PropertySource({"classpath:application.properties"}),
    @PropertySource({"classpath:messages.properties"})
})
public class SpringApplication {

    @Value("${database.jdbc.url}")
    private String jdbcUrl;

    @Bean
    public DataSource dataSource() {
        BasicDataSource ds = new BasicDataSource();
        //ds.setUrl("jdbc:mysql://45.120.136.152:3306/vrk2108");
        ds.setUrl(jdbcUrl);
        ds.setUsername("vrk2108");
        ds.setPassword("vrk2108");
        return ds;
    }
    
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringApplication.class);
        
        UserRepository userRepo = ctx.getBean(UserRepository.class);
        System.out.println(userRepo.getUserCount());
        
        //userRepo.getMatchedUsers("%a%").stream().forEach(System.out::println);
        //System.out.println(userRepo.getUserById(4));
        
//        User temp = new User(0, "Samuel", "samuel@mail.com", " ");
//        userRepo.addUser(temp);
        
        userRepo.deleteUser(5);
        userRepo.getUsers().stream().forEach(System.out::println);
    }

    public static void main8(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("test");
        ctx.register(PenFactory.class, SpringApplication.class);
        ctx.refresh();

        Pen a = ctx.getBean(Pen.class);
        System.out.println(a);

        UserController uc = ctx.getBean(UserController.class);
        uc.printJDBCProperties();
    }

    public static void main7(String[] args) throws Exception {
        AbstractApplicationContext ctx = new AnnotationConfigApplicationContext(SpringApplication.class);

        UserController userController = ctx.getBean(UserController.class);
        userController.fetchUsers().stream().forEach(System.out::println);
//        UserService service = ctx.getBean("userService", UserService.class);
//        service.populateUsers().stream().forEach(System.out::println);
    }

    public static void main6(String[] args) throws Exception {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

        UserController userController = ctx.getBean(UserController.class);

        userController.fetchUsers().stream().forEach(System.out::println);
    }

    public static void main5(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

        Pocket a = ctx.getBean("pocket2", Pocket.class);

        System.out.println(a);

        ctx.registerShutdownHook();
    }

    public static void main4(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

        User e1 = ctx.getBean("user4", User.class);
        System.out.println(e1);
    }

    public static void main3(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

        Engine e1 = ctx.getBean("engine4", Engine.class);
        System.out.println(e1);

    }

    public static void main2(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

        Car c1 = ctx.getBean("car3", Car.class);
        System.out.println(c1);
    }

    public static void main1(String[] args) throws Exception {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

        User u1 = ctx.getBean("user3", User.class);
        User u2 = ctx.getBean("user3", User.class);

        System.out.println(System.identityHashCode(u1));
        System.out.println(System.identityHashCode(u2));
        System.out.println(u1);
        System.out.println(u2);

        u2.setEmail("james.gosling@mail.com");
        System.out.println(u1);
        System.out.println(u2);

        ctx.registerShutdownHook();
    }
}
