package com.guruofjava.springcore;

import jdk.jfr.Label;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
public class PenFactory {

    @Bean(name = {"pen2"})
    @Primary
    @Profile("dev")
    public Pen parkerPen() {
        return new Pen("Parker", 25);
    }

    @Bean(name = {"pen1"})
    @Profile("test")
    public Pen reynoldsPen() {
        return new Pen("Reynolds", 18);
    }

    @Bean(name = {"pocket1"})
    @Lazy
    public Pocket createPocket(Pen pen) {
        return new Pocket(pen);
    }

    @Bean(name = {"pocket2"})
    @Lazy
    public Pocket createPocket() {
        return new Pocket();
    }
}
