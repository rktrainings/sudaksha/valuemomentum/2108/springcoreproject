package com.guruofjava.springcore;

import org.springframework.beans.factory.annotation.Required;

public class Pen {

    private String brand;
    private double price;

    public Pen() {
        super();
    }

    public Pen(String brand, double price) {
        this.brand = brand;
        this.price = price;
    }

    public void init() {
        System.out.println("Pen initialized");
    }

    public String getBrand() {
        return brand;
    }

    @Required
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Pen{" + "brand=" + brand + ", price=" + price + '}';
    }

}
