package com.guruofjava.springcore.user;

import com.guruofjava.springcore.User;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
//@Named
public class UserService {
    
    @Autowired
    private UserRepository userRepository;
    
    public List<User> populateUsers()throws SQLException{
        return userRepository.getAllUsers();
    }
}
