package com.guruofjava.springcore.user;

import com.guruofjava.springcore.User;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    //@Autowired
    //@Inject
    private UserService userService;
    
    @Autowired
    private Environment env;
    
    public void printJDBCProperties(){
        System.out.println(env.getProperty("database.jdbc.url"));
        System.out.println(env.getProperty("database.jdbc.username"));
        System.out.println(env.getProperty("input.username.label"));
    }

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public List<User> fetchUsers() throws SQLException {
        return userService.populateUsers();
    }
}
