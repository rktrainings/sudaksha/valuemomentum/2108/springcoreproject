package com.guruofjava.springcore.user;

import com.guruofjava.springcore.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    private UserRepository(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Integer getUserCount() {
        return jdbcTemplate.queryForObject("SELECT count(user_id) FROM users", Integer.class);
    }

    public void deleteUser(Integer userId) {
        jdbcTemplate.update("DELETE FROM users WHERE user_id=?", userId);
    }

    public void addUser(User user) {
        jdbcTemplate.update("INSERT INTO users VALUES(?, ?, ?, ?)",
                0, user.getName(), user.getEmail(), user.getRemarks());
    }

    public User getUserById(Integer userId) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE user_id=?",
                new UserRowMapper(), userId);
    }

    public List<User> getUsers() {

        return jdbcTemplate.query("SELECT * FROM users", new UserRowMapper());
    }

    public List<User> getMatchedUsers(String input) {
        return jdbcTemplate.query("SELECT * FROM users WHERE name like ?",
                new UserRowMapper(),
                input);
    }

    public List<User> getAllUsers() throws SQLException {
        Connection con = dataSource.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM users");

        List<User> users = new ArrayList();
        while (rs.next()) {
            users.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
        }
        return users;
    }

    public User getNewUser() {
        return new User();
    }
}
