package com.guruofjava.springcore;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;

public class Pocket {

//    @Autowired
//    @Qualifier("pen1")
    @Resource(name="pen1")
    private Pen pen;

    public Pocket() {
        super();
    }

    @Autowired
    public Pocket(Pen pen) {
        this.pen = pen;
    }
    
    @PostConstruct
    public void init(){
        System.out.println("After pocket constructed");
    }
    
    @PreDestroy
    public void destroy(){
        System.out.println("Before pocket destroyed");
    }

    public Pen getPen() {
        return pen;
    }

    //@Required
    public void setPen(Pen pen) {
        this.pen = pen;
    }

    @Override
    public String toString() {
        return "Pocket{" + "pen=" + pen + '}';
    }
}
